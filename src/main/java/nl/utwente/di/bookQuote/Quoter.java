package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private static final HashMap<String,Float> BOOK_PRICES = new HashMap<String,Float>();
    public Quoter(){
        BOOK_PRICES.put("1",10.0f);
        BOOK_PRICES.put("2",45.0f);
        BOOK_PRICES.put("3",20.0f);
        BOOK_PRICES.put("4",35.0f);
        BOOK_PRICES.put("5",50.0f);
    }

    /**
     * Returns the price for a given isbn
     * @param isbn the isbn of the book to check the price for
     * @return the price of the isbn
     */
    public float getBookPrice(String isbn) {
        Float price = BOOK_PRICES.get(isbn);
        return price == null? 0.0f: price;
    }
}
