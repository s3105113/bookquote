package nl.utwente.di.bookQuote;
import nl.utwente.di.bookQuote.Quoter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/** Tests the quoter */
public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("1");
        assertEquals(10.0,price,0.0);
    }
}
